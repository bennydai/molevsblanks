import os
import shutil
import random

directory = 'images/'

list_of_images = os.listdir(directory)

list_of_images.sort()

list_of_moles = []
list_of_blanks = []

for num in range(len(list_of_images)):
	if list_of_images[num][0] == 'm':
		list_of_moles.append(list_of_images[num])
	else:
		list_of_blanks.append(list_of_images[num])

mole_train_directory = 'data/train/mole/'
blank_train_directory = 'data/train/blanks/'
mole_validation_directory = 'data/validation/mole/'
blank_validation_directory = 'data/validation/blanks/'

random.shuffle(list_of_moles)
random.shuffle(list_of_blanks)

for num in range(0,30):
	shutil.copy(directory + list_of_moles[num], mole_train_directory + list_of_moles[num])

for num in range(30,45):
	shutil.copy(directory + list_of_moles[num], mole_validation_directory + list_of_moles[num])

for num in range(0,60):
	shutil.copy(directory + list_of_blanks[num], blank_train_directory + list_of_blanks[num])

for num in range(60,90):
	shutil.copy(directory + list_of_blanks[num], blank_validation_directory + list_of_blanks[num])