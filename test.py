import keras
import numpy as np

from keras.models import load_model, Model
from keras.preprocessing import image
import sys
from keras import backend as K
import matplotlib.pyplot as plt
from keract import get_activations, display_activations

# Load a pre-trained model
model = load_model('perception/mole_fourth.h5')

# Load the images into an array
list_of_images = ['perception/right.jpg', 'perception/center.jpg', 'perception/left.jpg']

# Compile the model using the parameters and optimisers that were used for training
model.compile(loss='binary_crossentropy',
              optimizer='adadelta',
              metrics=['accuracy'])



def load_image(img_path):
    img = image.load_img(img_path, target_size=(300, 300))  # loads the image and adjusts it to 300, 300 using keras preprocessing
    img_tensor = image.img_to_array(img)                    # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    img_tensor /= 255.                                      # imshow expects values in the range [0, 1] so we normalise it
    return img_tensor

# Preallocate an array of 3 for images, for some odd reason the predictions don't like being assigned by index so we append instead
images = [None] * 3
predictions = []

# For every image, we load it and predict whether it is an object of interest
for num in range(len(list_of_images)):
  images[num] = (load_image(list_of_images[num]))

for num in range(len(images)):
  pred = model.predict(images[num])
  predictions.append(pred[0][0].item())

print(model.summary())

a = get_activations(model, images[2])
display_activations(a)
# layer_name = 'conv2d_1'
# intermediate_layer_model = Model(inputs=model.input,
#                                  outputs=model.get_layer(layer_name).output)
# intermediate_output = intermediate_layer_model.predict(images[0])[0]
# output = np.array(intermediate_output)
# print(np.shape(output))
# plt.imshow(output[:][:][16])
# for num in range(len(images)):
# 	get_3rd_layer_output = K.function([model.layers[0].input], [model.layers[8].output])
# 	layer_output = get_3rd_layer_output([images[num]])[0]
# 	output_image = np.array(layer_output[0])
# 	print(np.shape(output_image))
# 	name = '%d.jpg' %(num)
# 	plt.imsave(name, output_image)

# Print out the predictions and pipe the argmax index to MATLAB
print(predictions)
sys.exit(int(np.argmax(predictions)+1))

